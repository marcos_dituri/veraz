<?php 

	class Veraz extends Controllers{
		public function __construct()
		{
			parent::__construct();
			session_start();			
			if(empty($_SESSION['login']))
			{
				header('Location: '.base_url().'/login');
			}
			getPermisos(3);
		}

		public function Veraz()
		{
			if(empty($_SESSION['permisosMod']['r'])){
				header("Location:".base_url().'/dashboard');
			}			
			$data['page_tag'] = "Consultar Veraz - Tienda Virtual";
			$data['page_title'] = "Consultar Veraz - Tienda Virtual";
			$data['page_name'] = "Veraz";
			$data['page_functions_js'] = "functions_veraz.js";
			$this->views->getView($this,"veraz",$data);
		}

		public function getVeraz(/*$strIdentificacion, $strNombre, $strSexo*/){
			if($_SESSION['permisosMod']['w']){
				if($_POST){		
					if(empty($_POST['txtIdentificacion']) || empty($_POST['txtNombre']) || empty($_POST['txtSexo'])){
						$arrResponse = array("status" => false, "msg" => 'Datos incorrectos.');
					}
					else {
						$strIdentificacion = strClean($_POST['txtIdentificacion']);
						$strNombre = strClean($_POST['txtNombre']);
						$strSexo = strClean($_POST['txtSexo']);		
		
						if($strIdentificacion > 7)
						{
							$arrData = $this->model->selectVeraz($strIdentificacion, $strNombre, $strSexo);
		
							if($arrData == false)
							{
								$arrResponse = array('status' => false, 'msg' => 'Datos mal cargados.');
							}else{
								$arrResponse = array('status' => true, 'msg' => $arrData);
							}
							echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
						}
					}										
				} 
			}
			die();
		}
	}
 ?>