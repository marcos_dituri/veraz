<?php


class VerazConsulta extends ApiVeraz
{
    function __construct()
    {
        $this->ApiVeraz = new ApiVeraz();
    }
    public function obtenerScore(string $identificacion, string $nombre, string $sexo)
    {
        $this->strIdentificacion = $identificacion;
        $this->strNombre = $nombre;
        $this->strSexo = $sexo;

        $consultaVeraz = $this->ConsultarApi($identificacion, $nombre, $sexo);

        return $consultaVeraz;
    }
    
};

?>
