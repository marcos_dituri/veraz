<?php
class ApiVeraz implements interfaceConsulta{
    
	public $respuesta;

	public function __construct(){
  
	}

    public function ConsultarApi($identificacion, $nombre, $sexo)
    {
        $archivo = 'Libraries/Core/xml/consulta_veraz_'.date("dmYHis").'.xml';

        $this->strIdentificacion = $identificacion;
        $this->strNombre = $nombre;
        $this->strSexo = $sexo;

        $headers = array (    
            'HTTP/1.1 200 OK',
            'Content-Type: text/html;charset=UTF-8',
            'Server: cloudflare-nginx',
            'Transfer-Encoding: chunked',
            'Connection: keep-alive'
        );
        
        include("xml/xml_envio.php");

        $ch = curl_init(URL_VERAZ);

        if (!$ch) {
            die("No se pudo inicializar la curl");
        }
        
        curl_setopt($ch, CURLOPT_URL, URL_VERAZ);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "par_xml=". $par_xml);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch); // Ejecutar

        //Guardadno archivo
        file_put_contents($archivo, $result);      //mostrar respuesta
        curl_close($ch);
        // Buscar archivo generado para  formatear
        $mensaje = new SimpleXMLElement(file_get_contents($archivo));
        $Error = $mensaje->estado->codigoError;
        // $producto = $mensaje->identificador[0]->producto;
        // $cliente = $mensaje->identificador[0]->client;

        if($Error != 0){
            $respuesta = false;
        } else {
            $Score = $mensaje->respuesta->integrante->variables->variable[14]->valor;
            if($Score < 200){
                $htmlEstado = '<span class="badge badge-primary">
                                <i class="fas fa-ban"></i>
                                <strong> DESAPROBADO!</strong></span>';
                $htmlScore = '<span class="badge badge-primary">'.$Score.'</span>';
            } else {
                $htmlEstado = '<span class="badge badge-success">
                                <i class="fas fa-check-circle"></i>
                                <strong>APROBADO</strong></span>';
                $htmlScore = '<span class="badge badge-success">'.$Score.'</span>';
            }

            $respuesta = '<div class="card">
                <div class="body project_report">
                    <div class="table-responsive">
                        <table class="table m-b-0 table-hover">
                            <thead>
                                <tr>
                                    <th>Score</th>
                                    <th>Datos</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        '.$htmlScore.'
                                    </td>
                                    <td class="project-title">
                                        <h6><a href="#">'.$identificacion.'</a></h6>
                                        <small>'.$nombre.'</small>
                                    </td>
                                    <td>
                                        '.$htmlEstado.'                        
                                    </td>
                                </tr>                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>';
        }

        return $respuesta;
    }   
}


?>