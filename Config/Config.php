<?php 
	
	//define("BASE_URL", "http://localhost/tienda_virtual/");
	const BASE_URL = "http://localhost/tienda_virtual/veraz/";

	//Zona horaria
	date_default_timezone_set('America/Guatemala');

	//Datos de conexión a Base de Datos
	const DB_HOST = "localhost";
	const DB_NAME = "db_tiendavirtual";
	const DB_USER = "root";
	const DB_PASSWORD = "";
	const DB_CHARSET = "utf8";

	//Deliminadores decimal y millar Ej. 24,1989.00
	const SPD = ".";
	const SPM = ",";

	//Simbolo de moneda
	const SMONEY = "Q";

	//Datos envio de correo
	const NOMBRE_REMITENTE = "Tienda Virtual";
	const EMAIL_REMITENTE = "no-reply@abelosh.com";
	const NOMBRE_EMPESA = "Tienda Virtual";
	const WEB_EMPRESA = "www.abelosh.com";

	//DATOS VERAZ
	const URL_VERAZ = "https://online.org.veraz.com.ar/pls/consulta817/wserv";
	const MATRIZ_VERAZ = "VN8123";
	const USUARIO_VERAZ = "XML";
	const PASSWORD_VERAZ = "55961059658293441126301465683";

	const PRODUCTO = "Experto";
	const SECTOR_VERAZ = "01";
	const SUCURSAL_VERAZ = "0";
	const FORMATO_INFORME_VERAZ = "T";
	const CLIENTE_VERAZ = "C28123";
	const FECHA_HORA_VERAZ = "YYYY-MM-DDTHH:MM:SS.099";
	

 ?>