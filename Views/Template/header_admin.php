<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Tienda Virtual Abel OSH">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Abel OSH">
    <meta name="theme-color" content="#009688">
    <link rel="shortcut icon" href="<?= media();?>/images/favicon.ico">
    <title><?= $data['page_tag'] ?></title>
    <!-- Main CSS-->
    <link rel="stylesheet" href="<?= media();?>/css/bootstrap.min.css">
    <!-- Custom Css -->
    <link rel="stylesheet" href="<?= media(); ?>/css/main.css">
    <link rel="stylesheet" href="<?= media(); ?>/css/color_skins.css">

    <!--<link rel="stylesheet" href="<?= media();?>/css/bootstrap-select.min.css"> -->
    <link rel="stylesheet" href="<?= media(); ?>/css/style.css">
    
  </head>
  
  <body class="theme-black menu_dark">
    <div id="divLoading" >
      <div>
        <img src="<?= media(); ?>/images/loading.svg" alt="Loading">
      </div>
    </div>
    <!-- Navbar-->
    <?php require_once("nav_admin.php"); ?> 


