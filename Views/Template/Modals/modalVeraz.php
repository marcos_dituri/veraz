<!-- Modal -->
<div class="modal fade" id="modalFormVeraz" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Nuva consulta</h4>
            </div>
            <form action="" id="formVeraz" name="formVeraz" class="form-horizontal">
                <div class="modal-body">
                    <div class="body">
                        <div class="demo-masked-input">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <b>DNI/CUIT</b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="far fa-id-card"></i> </span>
                                        <input name="txtIdentificacion" id="txtIdentificacion" type="text" class="form-control date" placeholder="Ej: 39374547">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12"> <b>Nombre y apellido</b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="far fa-user"></i></span>                                        
                                        <input name="txtNombre" id="txtNombre" type="text" class="form-control time24 form-control-danger" placeholder="Ej: David Jonathan">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12"> <b>Sexo</b>
                                    <div class="input-group">
                                        <div class="radio inlineblock m-r-20">
                                            <input type="radio" name="txtSexo" id="Masculino" class="with-gap" value="M" checked="">
                                            <label for="Masculino">Masucilo</label>
                                        </div>                                
                                        <div class="radio inlineblock">
                                            <input type="radio" name="txtSexo" id="Femenino" class="with-gap" value="F" >
                                            <label for="Femenino">Femenino</label>
                                        </div>
                                        <div class="radio inlineblock">
                                            <input type="radio" name="txtSexo" id="Sociedad" class="with-gap" value="S">
                                            <label for="Sociedad">Sociedad</label>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-default btn-round waves-effect">Enviar consulta</button>
                    <button type="button" class="btn btn-danger btn-simple btn-round waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="modalFormUsuariosss" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header headerRegister">
        <h5 class="modal-title" id="titleModal">Nuevo Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form id="formUsuario" name="formUsuario" class="form-horizontal">
              <input type="hidden" id="idUsuario" name="idUsuario" value="">
              <p class="text-primary">Todos los campos son obligatorios.</p>

              <div class="form-row">

              </div>
              <div class="form-row">

                <div class="form-group col-md-6">
                  <label for="txtApellido">Apellidos</label>
                  <input type="text" class="form-control valid validText" id="txtApellido2" name="txtApellido2" required="">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="txtTelefono">Teléfono</label>
                  <input type="text" class="form-control valid validNumber" id="txtTelefono" name="txtTelefono" required="" onkeypress="return controlTag(event);">
                </div>
                <div class="form-group col-md-6">
                  <label for="txtEmail">Email</label>
                  <input type="email" class="form-control valid validEmail" id="txtEmail" name="txtEmail" required="">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="listRolid">Tipo usuario</label>
                    <select class="form-control" data-live-search="true" id="listRolid" name="listRolid" required >
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="listStatus">Status</label>
                    <select class="form-control selectpicker" id="listStatus" name="listStatus" required >
                        <option value="1">Activo</option>
                        <option value="2">Inactivo</option>
                    </select>
                </div>
             </div>
             <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="txtPassword">Password</label>
                  <input type="password" class="form-control" id="txtPassword" name="txtPassword" >
                </div>
             </div>
              <div class="tile-footer">
                <button id="btnActionForm" class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i><span id="btnText">Guardar</span></button>&nbsp;&nbsp;&nbsp;
                <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cerrar</button>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

<!--MODAL VERAZ-->
