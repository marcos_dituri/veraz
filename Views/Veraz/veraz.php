<?php 
    headerAdmin($data); 
    getModal('modalVeraz',$data);
?>
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <h2><i class="fas fa-user-tag"></i> <?= $data['page_title'] ?></h2>
                    <ul class="breadcrumb padding-0">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Pages</a></li>
                        <li class="breadcrumb-item active"><?= $data['page_title'] ?></li>
                    </ul>
                </div>            
                <div class="col-lg-2 col-md-2 col-sm-12">
                    <div class="input-group m-b-0">      
                        <?php if($_SESSION['permisosMod']['w']){ ?>
                        <button type="button"  class="btn btn-primary" onclick="openModal();">
                        <i class="fas fa-plus-circle"></i> Nueva consulta
                        </button>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="header">
                <h2><strong>Masked</strong> Input</h2>
                <ul class="header-dropdown">
                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                            <li><a href="javascript:void(0);" class="boxs-close">Delete</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <?php
            function obtener_estructura_directorios($ruta){
                // Se comprueba que realmente sea la ruta de un directorio
                if (is_dir($ruta)){
                    // Abre un gestor de directorios para la ruta indicada
                    $gestor = opendir($ruta);
                    echo "<ul>";

                    // Recorre todos los elementos del directorio
                    while (($archivo = readdir($gestor)) !== false)  {
                            
                        $ruta_completa = $ruta . "/" . $archivo;

                        // Se muestran todos los archivos y carpetas excepto "." y ".."
                        if ($archivo != "." && $archivo != "..") {
                            // Si es un directorio se recorre recursivamente
                            if (is_dir($ruta_completa)) {
                                echo "<li>" . $archivo . "</li>";
                                obtener_estructura_directorios($ruta_completa);
                            } else {
                                echo "<li>" . $archivo . "</li>";
                            }
                        }
                    }
                    
                    // Cierra el gestor de directorios
                    closedir($gestor);
                    echo "</ul>";
                } else {
                    echo "No es una ruta de directorio valida<br/>";
                }
            }

            $ruta = 'Libraries/Core/xml';
            obtener_estructura_directorios($ruta)

            ?>
        </div>
    
    </div>
</section>
<?php footerAdmin($data); ?>
    