
let divLoading = document.querySelector("#divLoading");

document.addEventListener('DOMContentLoaded', function(){

    if(document.querySelector("#formVeraz")){
     
        let formVeraz = document.querySelector("#formVeraz");
        formVeraz.onsubmit = function(e) {
            e.preventDefault();
            let strIdentificacion = document.querySelector('#txtIdentificacion').value;
            let strNombre = document.querySelector('#txtNombre').value;
            let strSexo =document.querySelector('input[name="txtSexo"]:checked').value;
            

          
            if(strIdentificacion == '' || strNombre == '' || strSexo == '')
            {
                //swal("Atención", "Todos los campos son obligatorios." , "error");
                Swal.fire({
                    title: 'Atención!',
                    text: 'Todos los campos son obligatorios.',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                  })
                return false;
            } 

            let elementsValid = document.getElementsByClassName("valid");
            for (let i = 0; i < elementsValid.length; i++) { 
                if(elementsValid[i].classList.contains('is-invalid')) { 
                    swal("Atención", "Por favor verifique los campos en rojo." , "error");
                    return false;
                } 
            } 
            divLoading.style.display = "flex";
            let request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
            let ajaxUrl = base_url+'/Veraz/getVeraz'; 
            let formData = new FormData(formVeraz);
            request.open("POST",ajaxUrl,true);
            request.send(formData);
            request.onreadystatechange = function(){
                if(request.readyState == 4 && request.status == 200){
                    let objData = JSON.parse(request.responseText);
                    if(objData.status)
                    {
                        /*
                        if(rowTable == ""){
                            tableUsuarios.api().ajax.reload();
                        }else{
                            htmlStatus = intStatus == 1 ? 
                            '<span class="badge badge-success">Activo</span>' : 
                            '<span class="badge badge-danger">Inactivo</span>';
                            rowTable.cells[1].textContent = strNombre;
                            rowTable.cells[2].textContent = strApellido;
                            rowTable.cells[3].textContent = strEmail;
                            rowTable.cells[4].textContent = intTelefono;
                            rowTable.cells[5].textContent = document.querySelector("#listRolid").selectedOptions[0].text;
                            rowTable.cells[6].innerHTML = htmlStatus;
                            rowTable="";
                        }*/
                        $('#modalformVeraz').modal("hide");
                        formVeraz.reset();
                        swal.fire("Veraz encontrado", objData.msg ,"success");
                    }else{
                        swal.fire("Hubo un Error", objData.msg , "error");
                    }
                }
                divLoading.style.display = "none";
                return false;
            }
        }
    }
}, false);


window.addEventListener('load', function() {
    
}, false);





function openModal()
{
    $('#modalFormVeraz').modal('show');    
}

function openModalPerfil(){
    $('#modalFormPerfil').modal('show');
}