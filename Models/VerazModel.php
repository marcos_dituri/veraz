<?php 
class VerazModel extends VerazConsulta
{
	public $strIdentificacion;
	public $strNombre;
	public $strSexo;

	public function __construct()
	{
		parent::__construct();
	}	

	public function selectVeraz(string $identificacion, string $nombre, string $sexo){
		$this->strIdentificacion = $identificacion;
		$this->strNombre = $nombre;
		$this->strSexo = $sexo;
		$request = $this->obtenerScore($identificacion, $nombre, $sexo);		
		
		return $request;
	}

}

 ?>